﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create lists to contain the different types of animal
            List<IFlyer> flyers = new List<IFlyer>();
            List<IWalker> walkers = new List<IWalker>();
            List<ISlither> slitherers = new List<ISlither>();

            Random random = new Random();

            //Generate animals of different types based on index of loop
            for (int i = 0; i < 20; i++)
            {
                if (i < 5)
                {
                    Animal bird= new GlidingBird("glider" + i, random.NextDouble(), 2);
                    flyers.Add((IFlyer)bird);
                    walkers.Add((IWalker)bird);
                }
                else if (i < 10)
                {
                    Animal bird = new FlyingBird("flappy" + i, random.NextDouble(), 2);
                    flyers.Add((IFlyer)bird);
                    walkers.Add((IWalker)bird);
                }
                else if (i < 15)
                {
                    IWalker walker = new Pig("pig" + i, random.NextDouble(), 4);
                    walkers.Add(walker);
                }
                else if (i < 20)
                {
                    ISlither slitherer = new Snake("snek" + i, random.NextDouble(), 0);
                    slitherers.Add(slitherer);
                }
            }

            PrintCollectionsOfAnimals(flyers, walkers, slitherers);
        }

        private static void PrintCollectionsOfAnimals(List<IFlyer> flyers, List<IWalker> walkers, List<ISlither> slitherers)
        {
            //Print all the different collections of animals
            foreach (IWalker walker in walkers) walker.WalkByInterface();
            Console.WriteLine();
            foreach (ISlither slitherer in slitherers) slitherer.SlitherByInterface();
            Console.WriteLine();
            foreach (IFlyer flyer in flyers) flyer.FlyByInterface();
        }
    }
}
