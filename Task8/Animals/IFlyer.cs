﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8
{
    interface IFlyer
    {
        //methode to be implemented in classes that use this interface
        public void FlyByInterface();
      }
}
