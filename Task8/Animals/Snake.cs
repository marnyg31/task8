﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8
{
    class Snake : Animal,ISlither
    {
        public Snake(string name, double heightInMeters, int numLegs) : base(name, heightInMeters, numLegs)
        {
            //User base constructor
        }
        public Snake(string name, int numLegs) : base(name, numLegs)
        {
            //User base constructor
        }

        public override void Act()
        {
            //Print the action of the animal type
            Console.WriteLine(this.Name + " is slithiring around with " + this.NumLegs + " legs");
        }
        public void SlitherByInterface()
        {
            Console.WriteLine(this.Name + " is slithiring around with all ISlitherers");
        }

    }
}
