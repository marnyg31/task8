Solution for task 8

After running the program, 20 different animals will be generated. These animals will be different subtypes of animal. The classes that implement a interface, will be put into a list containing only objects that implement that specific interface.  

Once all animals are created, each collection of objects implementing that interface will be iterated over and perform the interface specific function.

The relevant code is found in /task8/Program.cs, and all animal classes and interfaces are placed in /task8/Animals/

